#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <opencv2/opencv.hpp>
#include "lib/parser/edvs_parser.h"
#include "lib/gabor/gabor_bank.h"
#include <stdio.h>
#include <QColor>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QImage>
#include <QLineF>
#include <QList>
#include <QLabel>
#include <QMainWindow>
#include <QPen>
#include <QPixmap>
#include <QPainter>
#include <QPoint>
#include <QSettings>
#include <QSize>
#include <QString>
#include <QThread>
#include <QTime>
#include <QTimer>
#include <QVector>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

/*
 * SIGNALS and SLOTS
 */
private slots:
  void on_actionLoadDefaults_triggered();
  void on_actionOpen_triggered();
  void on_actionQuit_triggered();
  void on_checkBoxAnalyseScene_toggled(bool toggled);
  void on_checkBoxBlurC1Results_clicked(bool checked);
  void on_checkBoxStreamEvents_clicked(bool checked);
  void on_gamma_valueChanged(double arg1);
  void on_maxLines_valueChanged(int arg1);
  void on_rbManualWeights_toggled(bool checked);
  void on_rbShowFilterResults_toggled(bool checked);
  void on_sliderFps_valueChanged(int position);
  void on_thresholdLines_valueChanged(int arg1);
  void on_weight0deg_valueChanged(double arg1);
  void on_weight45deg_valueChanged(double arg1);
  void on_weight90deg_valueChanged(double arg1);
  void on_weight135deg_valueChanged(double arg1);


public slots:
  void AnalyseScene();
  void LoadEdvsDataSuccess(bool ready);
  void ShowEvent(Edvs::Event event);
  void ShowEventStack(QVector<Edvs::Event> events);
  void ShowGaborFilterBank(QImage filter_bank);
  void ShowLines(QList<line2d> lines);
  void ShowUiMessage(QString message, int timeout);

signals:
  void GuiGaborFilterBlurdChanged(bool use_blur);
  void GuiGaborResultsShowModeChanged(bool show_filter_results);
  void GuiGammaChanged();
  void GuiMaxLinesChanged(int max_lines);
  void GuiOneWeightChanged(int orientation);
  void GuiWeightingFilterResultsChanged(bool use_manual_weights);
  void GuiThresholdChanged(int threshold);
  void SelectEdvsDumpFile(QString path);
  void ToggleEmittingEvents(bool emit_events);
  void TriggerImageAnalysis();

private:
  int             fps_;
  double          analysis_freq_ratio_ = 0.1;  // ]0, 1[
  int             max_lines_;
  QImage         *image_events_;
  QSize           edvs_frame_size_;
  QSize           edvs_source_size_;
  double          event_alpha_decay_;  // ]0, 1[
  QLabel         *label_live_view_;
  QPen            pen_lines_;
  QPixmap         pm_lines_;
  QPixmap         pm_events_;
  bool            stream_available_ = false;
  bool            updating_gui_ = false;
  Ui::MainWindow *ui;
  EdvsParser     *edvs_worker_;
  GaborBank      *gabor_bank_;
  QString         file_path_;
  QThread         edvs_thread_;
  QTimer         *analyse_timer_;
  /* Methods */
  void ApplyEventDecay(QImage *image, double decay);
  void InitEvents();
  void InitGaborBankTab();
  void InitLines();
  void InitLiveView();
  void InitPen();
  void InitSettings(bool update_gui = false);
  void LoadDefaults(bool force);
  void PaintOneEvent(Edvs::Event event);
  void StartEventStream();
  void StopEventStream();
  void UpdateAnalysisInterval();
  void UpdateLiveView();

  /*  Default settings  */
  struct defaut_settings {
    int fps = 30;
    int max_lines = 20;
    QSize edvs_source_size = QSize(128, 128);
    double event_alpha_decay = 0.9;
    double weight0deg = 0.8;
    double weight45deg = 0.5;
    double weight90deg = 1.0;
    double weight135deg = 0.5;
    bool use_manual_weights = false;
    bool show_filter_results = true;
    bool use_C1_blur = false;
    double gamma = 0.3;
    int threshold_lines = 150;
  };
};

#endif // MAINWINDOW_H
