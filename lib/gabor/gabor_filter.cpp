#include "gabor_filter.h"

GaborFilter::GaborFilter(QSize filter_size, QSize image_size, int orientation, float bandwidth, float wavelength, int id_f) :
  QObject(),
  filter_size_(filter_size),
  image_size_(image_size),
  orientation_(orientation),
  bandwidth_(bandwidth),
  wavelength_(wavelength),
  id_(id_f)
{
  QSettings settings;
  gamma_ = settings.value("gabor/gamma").toFloat();
  use_manual_weights_ = settings.value("gabor/use_manual_weights").toBool();
  show_filter_results_ = settings.value("gui/show_filter_results").toBool();
  blur_filter_results_ = settings.value("gabor/blurC1results").toBool();
  UpdateFilterBiases();
  InitializeFilter();
  filtered_image_ = Mat(image_size_.height(), image_size_.width(), CV_32F, Scalar::all(0));
  gabor_C1_survivors_ = Mat(image_size.height(), image_size.width(), CV_32F, Scalar::all(0));
}

GaborFilter::~GaborFilter()
{
  //
}

/*
 * SLOTS
 */
void GaborFilter::C1_1stMax()
{
  QElapsedTimer timer;
  timer.start();
  /* Find the local maxima  */
  Mat Max;
  dilate(filtered_image_, Max, getStructuringElement(MORPH_RECT, Point(15,15)));
  Max -= filtered_image_;
  Max.convertTo(Max, CV_8U);
  threshold(Max, Max, 0, 255, THRESH_BINARY_INV);
  gabor_C1_survivors_.setTo(0);
  filtered_image_.copyTo(gabor_C1_survivors_, Max);
  if (blur_filter_results_)
    blur(gabor_C1_survivors_, gabor_C1_survivors_, Size(5, 5));
  if (!show_filter_results_)
    emit FilterFinished(id_, gabor_C1_survivors_);
  emit C1_1stMaxFinished(id_, gabor_C1_survivors_);
}

void GaborFilter::FilterImage()
{
  Mat ones = Mat::ones(image_size_.height(), image_size_.width(), CV_32F);
  Mat filtered_black = Mat(image_size_.height(), image_size_.width(), CV_32F, Scalar::all(0));
  Mat filtered_white = Mat(image_size_.height(), image_size_.width(), CV_32F, Scalar::all(0));
  filter2D(image_black_, filtered_black, -1, filter_);
  filter2D(image_white_, filtered_white, -1, filter_);

  // TODO: fix remove shifting. Values should be in the range [0, 255]
  double min, max;
  minMaxLoc(filtered_black, &min, &max);
  divide(filtered_black - min, ones * (max - min), filtered_black);
  minMaxLoc(filtered_white, &min, &max);
  divide(filtered_white - min, ones * (max - min), filtered_white);

  /* Combine both filtered images. Each has values within a range of [0, 1]. To
   * get values in the range of [0, 255], they only have to be multiplied with a
   * factor of 128  */
  filtered_image_ = (filtered_black + filtered_white) * 128;
  if (use_manual_weights_)
      filtered_image_ *= filter_bias_;

  if (show_filter_results_)
    emit FilterFinished(id_, filtered_image_);
}

void GaborFilter::OnBlurUseChanged(bool use_blur)
{
  blur_filter_results_ = use_blur;
}

void GaborFilter::OnGammaChanged()
{
  QSettings settings;
  gamma_ = settings.value("gabor/gamma").toFloat();
  InitializeFilter();
}

void GaborFilter::OnWeightingFilterResultsChanged(bool use_manual_weights)
{
  use_manual_weights_ = use_manual_weights;
  InitializeFilter();
}

void GaborFilter::OnGaborFilterModePresentedChanged(bool show_filter_results)
{
  /* If show_filter_results is TRUE, then show the direct output of the filter
   * results. If show_filter_results is FALSE, show the results of the C1 Max
   * operation.                                                             */
  show_filter_results_ = show_filter_results;
}

void GaborFilter::UpdateImage(Mat image_black, Mat image_white)
{
  image_black.convertTo(image_black_, CV_32F);
  image_white.convertTo(image_white_, CV_32F);
}

void GaborFilter::UpdateFilterBiases()
{
  QSettings settings;
  switch (orientation_) {
    case 0:   filter_bias_ = settings.value("gabor/weights/0deg").toDouble(); break;
    case 45:  filter_bias_ = settings.value("gabor/weights/45deg").toDouble(); break;
    case 90:  filter_bias_ = settings.value("gabor/weights/90deg").toDouble(); break;
    case 135: filter_bias_ = settings.value("gabor/weights/135deg").toDouble(); break;
    default:  qWarning().noquote() << InstanceID()
                                   << "No matching filter orientation found when updating the filter biases.";
              break;
    }
}

/*
 * PRIVATE Methods
 */
void GaborFilter::AssureMatRange(Mat *mat, int min, int max)
{
  double min_f, max_f;
  minMaxLoc(*mat, &min_f, &max_f);
  if (min_f < min || max_f > max)
    qDebug().noquote() << InstanceID() << QString("Min/Max allowed: %1/%2").arg(min).arg(max)
                       << QString("Min/Max found: %1/%2").arg(min_f).arg(max_f);
}

void GaborFilter::InitializeFilter()
{
  // TODO: fix Gabor Filters are not normalized
  filter_ = Mat(filter_size_.height(), filter_size_.width(), CV_32F, Scalar::all(0));
  int a = (filter_size_.height() - 1) / 2;
  int b = (filter_size_.width()  - 1) / 2;
  float sin_t = sin(-(orientation_ - 90) * M_PI / 180.0);
  float cos_t = cos(-(orientation_ - 90) * M_PI / 180.0);
  float pi2_l = 2 * M_PI / wavelength_;
  for (int x = -a; x <= +a; x ++)
    for (int y = -b; y <= +b; y++) {
        float x0 = +x * cos_t + y * sin_t;
        float y0 = -x * sin_t + y * cos_t;
        /*  Use the even Gabor filter */
        filter_.at<float>(x + a, y + b) = exp((-pow(x0, 2.0) - pow(gamma_, 2.0) * pow(y0, 2.0))
                                          / (2 * pow(bandwidth_, 2.0))) * cos(pi2_l * x0);
      }
}

QString GaborFilter::InstanceID()
{
  return QString("GaborFilter (Orientation: %1° | FilterSize: %2x%3)").arg(orientation_, 3).arg(filter_size_.height(), 2).arg(filter_size_.width());
}
