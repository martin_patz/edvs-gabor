#ifndef GABOR_FILTER_H
#define GABOR_FILTER_H

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <math.h>
#include <QDebug>
#include <QElapsedTimer>
#include <QImage>
#include <QList>
#include <QPoint>
#include <QSettings>
#include <QSize>
#include <QString>

using namespace cv;

class GaborFilter : public QObject
{
  Q_OBJECT

public:
  GaborFilter(QSize size, QSize image_size, int orientation, float bandwidth, float wavelength, int id_f);
  virtual ~GaborFilter();

public slots:
  void C1_1stMax();
  void FilterImage();
  void OnBlurUseChanged(bool use_blur);
  void OnGammaChanged();
  void OnWeightingFilterResultsChanged(bool use_manual_weights);
  void OnGaborFilterModePresentedChanged(bool show_filter_results);
  void UpdateImage(Mat image_black, Mat image_white);
  void UpdateFilterBiases();

signals:
  void C1_1stMaxFinished(int id, Mat survivors);
  void FilterFinished(int id, Mat filtered_image);

private:
  /*  Properties  */
  bool use_manual_weights_;
  bool show_filter_results_;
  bool blur_filter_results_;
  double filter_bias_;
  float gamma_;  // aspect ratio
  float bandwidth_;
  float wavelength_;
  int orientation_;
  int id_;
  QSize filter_size_;
  QSize image_size_;
  Mat filter_;
  Mat filtered_image_;
  Mat image_black_;
  Mat image_white_;
  Mat gabor_C1_survivors_;

  /*  Methods     */
  void AssureMatRange(Mat* mat, int min = 0, int max = 255);
  void InitializeFilter();
  QString InstanceID();
};

#endif // GABOR_FILTER_H
