#include "gabor_bank.h"

GaborBank::GaborBank(QSize edvs_source_size, int max_lines) :
  QObject(),
  edvs_source_size_(edvs_source_size),
  max_lines_(max_lines),
  nr_filters_(FilterSizes.size()),
  nr_orientations_(Orientations.size())
{
  int id_f = 0;
  for (int o = 0; o < nr_orientations_; ++ o)
    for (int f = 0; f < nr_filters_; ++ f) {
        GaborFilter* gabor_filter = new GaborFilter(
              FilterSizes.at(f),
              edvs_source_size_,
              Orientations.at(o),
              Bandwidths.at(f),
              Wavelengths.at(f),
              id_f);
        gabor_threads_.append(new QThread());
        gabor_filters_.append(gabor_filter);
        gabor_filters_.last()->moveToThread(gabor_threads_.last());
        connect(gabor_threads_.last(), &QThread::finished,
                gabor_filters_.last(), &QObject::deleteLater);
        connect(this, &GaborBank::TriggerC1_1stMax,
                gabor_filters_.last(), &GaborFilter::C1_1stMax);
        connect(gabor_filters_.last(), &GaborFilter::C1_1stMaxFinished,
                this, &GaborBank::ReceiveSurvivors);
        connect(this, &GaborBank::UpdateFilterImages,
                gabor_filters_.last(), &GaborFilter::UpdateImage);
        connect(this, &GaborBank::TriggerFilter,
                gabor_filters_.last(), &GaborFilter::FilterImage);
        connect(gabor_filters_.last(), &GaborFilter::FilterFinished,
                this, &GaborBank::ReceiveFilteredImage);
        connect(this, &GaborBank::WeightingFilterResultsChanged,
                gabor_filters_.last(), &GaborFilter::OnWeightingFilterResultsChanged);
        connect(this, &GaborBank::GammaChanged,
                gabor_filters_.last(), &GaborFilter::OnGammaChanged);
        connect(this, &GaborBank::GaborResultsShowModeChanged,
                gabor_filters_.last(), &GaborFilter::OnGaborFilterModePresentedChanged);
        connect(this, &GaborBank::BlurUseChanged,
                gabor_filters_.last(), &GaborFilter::OnBlurUseChanged);
        switch (Orientations.at(o)) {
          case 0:   connect(this, &GaborBank::WeightChanged_0,
                            gabor_filters_.last(), &GaborFilter::UpdateFilterBiases);
                    break;
          case 45:  connect(this, &GaborBank::WeightChanged_45,
                            gabor_filters_.last(), &GaborFilter::UpdateFilterBiases);
                    break;
          case 90:  connect(this, &GaborBank::WeightChanged_90,
                            gabor_filters_.last(), &GaborFilter::UpdateFilterBiases);
                    break;
          case 135: connect(this, &GaborBank::WeightChanged_135,
                            gabor_filters_.last(), &GaborFilter::UpdateFilterBiases);
                    break;
          default:  DebugWarningInvalidOrientation(Orientations.at(o), "Connecting Signals/Slots failed."); break;
          }

        gabor_threads_.last()->start();
        gabor_responses_.append(Mat(edvs_source_size_.height(), edvs_source_size_.width(), CV_8U));
        gabor_C1_survivors_.append(Mat(edvs_source_size_.height(), edvs_source_size_.width(), CV_32F));
        id_f++;
  }
  threads_total_ = id_f;
  /*  Init Timers */
  filter_timer_ = QElapsedTimer();
  C1_timer_ = QElapsedTimer();
  C2_timer_ = QElapsedTimer();
}

GaborBank::~GaborBank()
{
  qDeleteAll(gabor_filters_.begin(), gabor_filters_.end());
  QList<QThread*>::const_iterator i;
  for(i = gabor_threads_.constBegin(); i != gabor_threads_.constEnd(); ++i)
    (*i)->quit();
  for(i = gabor_threads_.constBegin(); i != gabor_threads_.constEnd(); ++i)
    (*i)->wait();
}

void GaborBank::C1_2ndMax()
{
  gabor_C2_survivors_ = QMultiMap<int, line2d>();
  for (int row = 0; row < edvs_source_size_.height(); row++)
    for (int col = 0; col < edvs_source_size_.width(); col++) {
        int max = 0;
        line2d survivor_info;
        for (int f = nr_filters_ - 1; f >= 0; f--)
          for (int o = 0; o < nr_orientations_; o++) {
              int idx = o * nr_filters_ + f;
              if (gabor_C1_survivors_[idx].at<int>(row, col) >= max) {
                  max = gabor_C1_survivors_[idx].at<int>(row, col);
                  survivor_info.value = max;
                  survivor_info.location = QPoint(row, col);
                  survivor_info.orientation = Orientations[o];
                  survivor_info.filter_size = FilterSizes.at(f);
                  // TODO: fix maybe bug here when selecting the survivors
                }
            }
        /* A survivor for this pixel has been found */
        if (max > threshold_)
          gabor_C2_survivors_.insert(max, survivor_info);
      }
  /*  Find the best n (max_lines_) samples  */
  int line_counter = max_lines_;
  QMapIterator<int, line2d> it (gabor_C2_survivors_);
  it.toBack();
  lines_.clear();
  while (it.hasPrevious() & line_counter > 0) {
      it.previous();
      lines_.append(it.value());
      line_counter--;
    }
}

void GaborBank::UpdateImage(QImage *new_image)
{
  if (still_analysing_)
    return;
  Mat converted_image_black(edvs_source_size_.height(), edvs_source_size_.width(), CV_8UC1, Scalar::all(0));
  Mat converted_image_white(edvs_source_size_.height(), edvs_source_size_.width(), CV_8UC1, Scalar::all(0));
  QRgb pixel;
  for (int x = 0; x < new_image->height(); ++ x)
    for (int y = 0; y < new_image->width(); ++ y) {
        pixel = new_image->pixel(x, y);
        if (qRed(pixel) == 255) // darker event registered
          converted_image_black.at<int>(x, y) = qAlpha(pixel);
        else // brighter event registered
          converted_image_white.at<int>(x, y) = qAlpha(pixel);
      }
  emit UpdateFilterImages(converted_image_black, converted_image_white);
}

void GaborBank::DebugWarningInvalidOrientation(int orientation, QString additional_msg)
{
  qWarning().noquote() << "GaborBank: Orientation" << orientation
                       << "is not in the set of valid orientations."
                       << additional_msg;
}

void GaborBank::ShowFilterResponses()
{
  QImage img = QImage(nr_filters_ * edvs_source_size_.width(),
                      nr_orientations_ * edvs_source_size_.height(), QImage::Format_RGB32);
  for(int o = 0; o < nr_orientations_; o ++)
    for (int f = 0; f < nr_filters_; f ++) {
        int idx_f = o * nr_filters_ + f;
        int idx_img_x = f * edvs_source_size_.width();
        int idx_img_y = o * edvs_source_size_.height();
        for (int x = 0; x < edvs_source_size_.width(); x ++)
          for (int y = 0; y < edvs_source_size_.height(); y ++) {
              int px_val = gabor_responses_.at(idx_f).at<int>(x, y);
              if (px_val == 255)
                img.setPixel(idx_img_x + x, idx_img_y + y, qRgb(0, px_val, 0));
              else if (px_val > 230)
                img.setPixel(idx_img_x + x, idx_img_y + y, qRgb(px_val, px_val, 0));
              else
                img.setPixel(idx_img_x + x, idx_img_y + y, qRgb(px_val, px_val, px_val));
            }
      }
  emit UpdateGaborBank(img);
}

/*
 * SLOTS
 */
void GaborBank::Analyse()
{
  if (still_analysing_)
    return;
  threads_filtering_left_ = threads_total_;
  still_filtering_ = true;
  filter_timer_.start();
  emit TriggerFilter();
  /*  Apply Max Operations  */
  threads_C1_Max_left_ = threads_total_;
  still_analysing_ = true;
  emit TriggerC1_1stMax();
}

void GaborBank::OnGaborFilterBlurChanged(bool use_blur)
{
  emit BlurUseChanged(use_blur);
}

void GaborBank::OnGaborResultsShowModeChanged(bool show_filter_results)
{
  emit GaborResultsShowModeChanged(show_filter_results);
}

void GaborBank::OnGammaChanged()
{
  emit GammaChanged();
}

void GaborBank::OnMaxLinesChanged(int max_lines)
{
  max_lines_ = max_lines;
}

void GaborBank::OnWeightChanged(int orientation)
{
  switch (orientation) {
    case 0:   emit WeightChanged_0();   break;
    case 45:  emit WeightChanged_45();  break;
    case 90:  emit WeightChanged_90();  break;
    case 135: emit WeightChanged_135(); break;
    default:  DebugWarningInvalidOrientation(orientation);
              break;
    }
}

void GaborBank::OnThresholdChanged(int threshold)
{
  threshold_ = threshold;
}

void GaborBank::ReceiveFilteredImage(int id, Mat filtered_image)
{
  filtered_image.convertTo(filtered_image, CV_8U);
  gabor_responses_[id] = filtered_image;
  threads_filtering_left_--;
  if (threads_filtering_left_ == 0) {
      qDebug() << "Filtering images took" << filter_timer_.elapsed()
               << "ms.";
      C1_timer_.start();
      ShowFilterResponses();
      still_filtering_ = false;
    }
}

void GaborBank::ReceiveSurvivors(int id, Mat survivors)
{
  gabor_C1_survivors_[id] = survivors;
  threads_C1_Max_left_--;
  if (threads_C1_Max_left_ == 0) {
      qDebug() << "C1 1st Max operation took" << C1_timer_.elapsed()
               << "ms.";
      C2_timer_.start();
      C1_2ndMax();
      qDebug() << "C2 2nd Max operation took" << C2_timer_.elapsed()
               << "ms.";
      emit UpdateLines(lines_);
      still_analysing_ = false;
    }
}

void GaborBank::OnWeightingFilterResultsChanged(bool use_manual_weights)
{
  emit WeightingFilterResultsChanged(use_manual_weights);
}
