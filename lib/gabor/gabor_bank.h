#ifndef GABOR_BANK_H
#define GABOR_BANK_H

#include "lib/gabor/gabor_filter.h"
#include <opencv2/opencv.hpp>
#include <QDebug>
#include <QElapsedTimer>
#include <QImage>
#include <QList>
#include <QMapIterator>
#include <QMultiMap>
#include <QSize>
#include <QString>
#include <QThread>
#include <QVector>

using namespace cv;

struct line2d {
  int value;
  QPoint location;
  int orientation;
  QSize filter_size;
  friend bool operator< (const line2d& x, const line2d& y) { return x.value < y.value; };
};

class GaborBank : public QObject
{
  Q_OBJECT

public:
  GaborBank(QSize edvs_source_size, int max_lines);
  virtual ~GaborBank();
  void UpdateImage(QImage *new_image);
  
  /*  Properties  #/
   *  Can be calculated with a separate Matlab script
   */
  const QList<QSize> FilterSizes {QSize(5, 5), QSize(9, 9), QSize(13, 13), QSize(17, 17), QSize(21, 21), QSize(25, 25)};
  const QList<float> Bandwidths {1.2900, 3.3409, 5.3918, 7.4426, 9.4935, 11.5444};
  const QList<float> Wavelengths {1.6450, 4.2021, 6.7591, 9.3162, 11.8732, 14.4303};
  /*  Define orientations yourself */
  const QList<int> Orientations {0, 45, 90, 135};

private:
  /*  Properties  */
  bool still_analysing_ = false, still_filtering_ = false;
  int nr_filters_, nr_orientations_, max_lines_;
  int threads_C1_Max_left_, threads_filtering_left_, threads_total_;
  int threshold_;
  QSize edvs_source_size_;
  QList<GaborFilter*> gabor_filters_;
  QList<QThread*> gabor_threads_;
  QList<Mat_<int>> gabor_responses_;
  QList<Mat_<int>> gabor_C1_survivors_;
  QMultiMap<int, line2d> gabor_C2_survivors_;
  QList<line2d> lines_;
  QElapsedTimer filter_timer_, C1_timer_, C2_timer_;

  /*  Methods     */
  void C1_2ndMax();
  void ConnectSignalsSlots();
  void DebugWarningInvalidOrientation(int orientation, QString additional_msg = "");
  void ShowFilterResponses();

public slots:
  void Analyse();
  void OnGaborFilterBlurChanged(bool use_blur);
  void OnGaborResultsShowModeChanged(bool show_filter_results);
  void OnGammaChanged();
  void OnMaxLinesChanged(int max_lines);
  void OnWeightChanged(int orientation);
  void OnWeightingFilterResultsChanged(bool use_manual_weights);
  void OnThresholdChanged(int threshold);
  void ReceiveFilteredImage(int id, Mat filtered_image);
  void ReceiveSurvivors(int id, Mat survivors);

signals:
  void GaborResultsShowModeChanged(bool show_filter_results);
  void GammaChanged();
  void BlurUseChanged(bool use_blur);
  void WeightingFilterResultsChanged(bool use_manual_weights);
  void WeightChanged_0();
  void WeightChanged_45();
  void WeightChanged_90();
  void WeightChanged_135();
  void TriggerC1_1stMax();
  void TriggerFilter();
  void UpdateFilterImages(Mat image_black, Mat image_white);
  void UpdateGaborBank(QImage filter_bank);
  void UpdateLines(QList<line2d> lines);
};

#endif // GABOR_BANK_H



