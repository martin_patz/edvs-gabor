#ifndef INCLUDE_PARSER_H
#define INCLUDE_PARSER_H

#include "lib/edvstools/Edvs/Event.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <QDebug>
#include <QString>
#include <QTimer>

class EdvsParser : public QObject
{
  Q_OBJECT
/**
 *  PUBLIC METHODS
 */
public:
  explicit EdvsParser(int fps);
  ~EdvsParser();
  bool EventsLeft();
  bool EventListLoaded();
  std::vector<Edvs::Event>* GetAll();
  Edvs::Event GetOne();
  void SelectFile(QString path);
  void ParseFile();
  bool PopOne(Edvs::Event& event);
  void SetEmittingSpeed(int fps);

/**
 *  PRIVATE METHODS
 */
private:
  Edvs::Event MakeEvent(std::vector<std::string> line_array);  // Transform to event

/**
 *  PRIVATE PROPERTIES
 */
private:
  bool emit_events_ = false;
  int current_event_index_;
  int update_frequency_ = 30;
  double time_window_ = 1.0;
  int fps_ = 30;
  int num_events_;
  uint64_t  current_time_ = 0;
  std::string source_file_path_;
  std::ifstream infile_;
  std::vector<Edvs::Event> event_list_;
  QTimer *event_push_timer_;
  /*  Methods */

/*
 * SIGNALS and SLOTS
 */
signals:
  void PushOne(const Edvs::Event &event);
  void PushStack(QVector<Edvs::Event> &events);
  void LoadEdvsDataSuccess(bool success);
  void ShowUiMessage(QString message, int timeout = 2000);

public slots:
  void ChangeFps(int fps);
  void PushEvents();
  void PushEventStack();
  void SelectAndParseFile(QString path);
  void ToggleEmittingEvents(bool emit_events);
};

#endif
