#include "edvs_parser.h"

/**
 *  PUBLIC METHODS
 */

EdvsParser::EdvsParser(int fps) : QObject()
{
  ChangeFps(fps);
  std::string empty_string;
  current_event_index_ = 0;
  source_file_path_ = empty_string;
  event_push_timer_ = new QTimer(this);
  connect(event_push_timer_, &QTimer::timeout,
          this, &EdvsParser::PushEventStack);
}

EdvsParser::~EdvsParser()
{
  qDebug() << "Shutting down parser.";
  qDebug() << "Timer is active:" << event_push_timer_->isActive();
  infile_.close();
}

bool EdvsParser::EventListLoaded()
{
  return !source_file_path_.empty();
}

bool EdvsParser::EventsLeft()
{
  return event_list_.size() != 0;
}

std::vector<Edvs::Event> *EdvsParser::GetAll()
{
  return &event_list_;
}

Edvs::Event EdvsParser::GetOne()
{
  current_event_index_ = (current_event_index_ + 1) % num_events_;
  return event_list_.at(current_event_index_);
}

void EdvsParser::ParseFile()
{   // parses file and stores it in the event list
  std::string delimiter = "\t";
  std::string line;
  std::vector<std::string> line_array;
  event_list_.clear();
  emit ShowUiMessage("Loading eDVS dump file...", 0);
  while(std::getline(infile_, line)) {
    // Seperate strings by delimiter
    boost::split(line_array, line, boost::is_any_of(delimiter.c_str()));
    // Transform seperated strings to event and add them to event list
    event_list_.push_back(MakeEvent(line_array));
  }
  num_events_ = event_list_.size();
  current_event_index_ = 0;
  qDebug() << "Successfully loaded " + QString::number(num_events_) + " events from " + QString::fromStdString(source_file_path_);
  emit ShowUiMessage("Successfully loaded " + QString::number(num_events_) + " events");
}

bool EdvsParser::PopOne(Edvs::Event& event)
{
  if( !event_list_.empty() ) {
      event = event_list_.back();
      event_list_.pop_back();
      return true;
    }
  else
    return false;
}

void EdvsParser::SelectFile(QString path)
{
  source_file_path_ = path.toStdString();
  event_list_.clear();

  if(infile_.is_open())
    infile_.close();
  infile_.open((source_file_path_).c_str());
}

/*
 * SLOTS
 */
void EdvsParser::ChangeFps(int fps)
{
  fps_ = fps;
  time_window_ = fps_ * 10000.0 / update_frequency_;
}

void EdvsParser::PushEvents()
{
  while(event_list_.at(current_event_index_).t < current_time_ + time_window_
     && event_list_.at(current_event_index_).t >= current_time_) {
      emit PushOne(event_list_.at(current_event_index_));
      current_event_index_ = (current_event_index_ + 1) % num_events_;
    }
  if (event_list_.at(current_event_index_).t < current_time_
      || current_time_ == 0)
    // jumping back to the beginning of the dump file
    current_time_ = event_list_.at(current_event_index_).t;
  else
    current_time_ += time_window_;
}

void EdvsParser::PushEventStack()
{
  QVector<Edvs::Event> events;
  while(event_list_.at(current_event_index_).t < current_time_ + time_window_
     && event_list_.at(current_event_index_).t >= current_time_) {
      events.append(event_list_.at(current_event_index_));
      current_event_index_ = (current_event_index_ + 1) % num_events_;
    }
  if (event_list_.at(current_event_index_).t < current_time_
      || current_time_ == 0)
    current_time_ = event_list_.at(current_event_index_).t;
  else
    current_time_ += time_window_;
  emit PushStack(events);
}

void EdvsParser::SelectAndParseFile(QString path)
{
  try {
    SelectFile(path);
    ParseFile();
    emit LoadEdvsDataSuccess(true);
  } catch (std::exception &e) {
    emit ShowUiMessage("Error: " + QString(e.what()));
    emit LoadEdvsDataSuccess(false);
  }
}

void EdvsParser::ToggleEmittingEvents(bool emit_events)
{
  emit_events_ = emit_events;
  if (emit_events_)
    event_push_timer_->start(1000.0 / update_frequency_);
  else
    event_push_timer_->stop();
}

/**
 *  PRIVATE METHODS
 */

Edvs::Event EdvsParser::MakeEvent(std::vector<std::string> line_array)
{
  Edvs::Event event;
  event.t = boost::lexical_cast<uint64_t>(line_array[0]);
  event.x = boost::lexical_cast<uint16_t>(line_array[1]);
  event.y = boost::lexical_cast<uint16_t>(line_array[2]);
  event.parity = boost::lexical_cast<bool>(line_array[3]);
  event.id = boost::lexical_cast<uint8_t>(line_array[4]);
  return event;
}
