#include "mainwindow.h"
#include "ui_mainwindow.h"

Q_DECLARE_METATYPE(Edvs::Event)
Q_DECLARE_METATYPE(QVector<Edvs::Event>)
Q_DECLARE_METATYPE(Mat)

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  analyse_timer_(new QTimer(this))
{
  /*
   * Register Meta Types to be able to be passed through Signals and Slots
  */
  qRegisterMetaType< Edvs::Event >();
  qRegisterMetaType< QVector<Edvs::Event> >("QVector<Edvs::Event>&");
  qRegisterMetaType< Mat >();
  /*
   * Setup UI
  */
  ui->setupUi(this);
  /*
   * Load Settings
   */
  /*  Set application wide settings, allowing a more simple syntax to access the QSettings  */
  QCoreApplication::setOrganizationName("TUM NST");
  QCoreApplication::setApplicationName("eDVS Stream-Display and Gabor-Filter");
  QCoreApplication::setOrganizationDomain("martin-patz.de/academic/forschungspraxis_tum.html");
  /*  Read settings and initialize default values */
  LoadDefaults(false);
  InitSettings();
  /* Initialize the Edvs Dump File / Sensor Parser */
  edvs_worker_ = new EdvsParser(fps_);
  gabor_bank_ = new GaborBank(edvs_source_size_, max_lines_);

  /*
   *  Launch EdvsParser in a separate Thread
  */
  /*  Connect with gabor filter bank  */
  connect(analyse_timer_, &QTimer::timeout,
          this, &MainWindow::AnalyseScene);
  connect(this, &MainWindow::TriggerImageAnalysis,
          gabor_bank_, &GaborBank::Analyse);
  connect(gabor_bank_, &GaborBank::UpdateLines,
          this, &MainWindow::ShowLines);
  connect(gabor_bank_, &GaborBank::UpdateGaborBank,
          this, &MainWindow::ShowGaborFilterBank);
  connect(this, &MainWindow::GuiWeightingFilterResultsChanged,
          gabor_bank_, &GaborBank::OnWeightingFilterResultsChanged);
  connect(this, &MainWindow::GuiOneWeightChanged,
          gabor_bank_, &GaborBank::OnWeightChanged);
  connect(this, &MainWindow::GuiGammaChanged,
          gabor_bank_, &GaborBank::OnGammaChanged);
  connect(this, &MainWindow::GuiThresholdChanged,
          gabor_bank_, &GaborBank::OnThresholdChanged);
  connect(this, &MainWindow::GuiMaxLinesChanged,
          gabor_bank_, &GaborBank::OnMaxLinesChanged);
  connect(this, &MainWindow::GuiGaborResultsShowModeChanged,
          gabor_bank_, &GaborBank::OnGaborResultsShowModeChanged);
  connect(this, &MainWindow::GuiGaborFilterBlurdChanged,
          gabor_bank_, &GaborBank::OnGaborFilterBlurChanged);
  /*  Connect with EDVS parser        */
  edvs_worker_->moveToThread(&edvs_thread_);
  connect(&edvs_thread_, &QThread::finished,
          edvs_worker_, &QObject::deleteLater);
  connect(this, &MainWindow::SelectEdvsDumpFile,
          edvs_worker_, &EdvsParser::SelectAndParseFile,
          Qt::QueuedConnection);
  connect(this, &MainWindow::ToggleEmittingEvents,
          edvs_worker_, &EdvsParser::ToggleEmittingEvents);
  connect(edvs_worker_, &EdvsParser::ShowUiMessage,
          this, &MainWindow::ShowUiMessage);
  connect(edvs_worker_, &EdvsParser::LoadEdvsDataSuccess,
          this, &MainWindow::LoadEdvsDataSuccess);
  connect(edvs_worker_, &EdvsParser::PushStack,
          this, &MainWindow::ShowEventStack);
  connect(ui->sliderFps, &QSlider::valueChanged,
          edvs_worker_, &EdvsParser::ChangeFps);
  edvs_thread_.start();

  InitLiveView();
  InitEvents();
  InitLines();
  InitPen();
  UpdateLiveView();
  InitGaborBankTab();
}

MainWindow::~MainWindow()
{
  delete ui;
  delete label_live_view_;
  delete image_events_;
  edvs_thread_.quit();
  edvs_thread_.wait();
  delete gabor_bank_;
}

/*
 * SLOTS
 */
void MainWindow::on_actionLoadDefaults_triggered()
{
  LoadDefaults(true);
}

void MainWindow::on_actionQuit_triggered()
{
  close();
}

void MainWindow::on_actionOpen_triggered()
{
  file_path_ = QFileDialog::getOpenFileName(
    this
    ,tr("Open Event Dump File")
    ,QDir::homePath()
    ,tr("Event Dump File (*.tsv)")
    );
  qDebug() << file_path_;
  if(!file_path_.isEmpty()) {
    emit ToggleEmittingEvents(false);
    emit SelectEdvsDumpFile(file_path_);
    }
}

void MainWindow::on_checkBoxAnalyseScene_toggled(bool toggled)
{
  if (toggled)
    analyse_timer_->start();
  else
    analyse_timer_->stop();
}

void MainWindow::on_checkBoxBlurC1Results_clicked(bool checked)
{
  QSettings settings;
  settings.setValue("gabor/blurC1results", checked);
  emit GuiGaborFilterBlurdChanged(checked);
}

void MainWindow::on_checkBoxStreamEvents_clicked(bool checked)
{
  if (!stream_available_)
    ui->statusBar->showMessage("No event stream available. Connect eDVS sensor or open dump file.", 3000);
  else if (checked) {
      qDebug() << "Checkbox Stream Events is checked";
      StartEventStream();
    }
  else {
      qDebug() << "Checkbox Stream Events is not checked";
      StopEventStream();
    }
}

void MainWindow::on_gamma_valueChanged(double arg1)
{
    QSettings settings;
    settings.setValue("gabor/gamma", arg1);
    emit GuiGammaChanged();
}

void MainWindow::on_maxLines_valueChanged(int arg1)
{
  QSettings settings;
  settings.setValue("gabor/max_lines", arg1);
  emit GuiMaxLinesChanged(arg1);
}

void MainWindow::on_rbManualWeights_toggled(bool checked)
{
  QSettings settings;
  settings.setValue("gabor/use_manual_weights", checked);
  if (checked)
    ui->rbManualWeights->setChecked(true);
  else
    ui->rbAutomatedBalancing->setChecked(true);
  ui->weight0deg->setEnabled(checked);
  ui->weight45deg->setEnabled(checked);
  ui->weight90deg->setEnabled(checked);
  ui->weight135deg->setEnabled(checked);
  emit GuiWeightingFilterResultsChanged(checked);
}

void MainWindow::on_rbShowFilterResults_toggled(bool checked)
{
  QSettings settings;
  settings.setValue("gui/show_filter_results", checked);
  if (checked)
    ui->rbShowFilterResults->setChecked(true);
  else
    ui->rbShowC1MaxResults->setChecked(true);
  emit GuiGaborResultsShowModeChanged(checked);
}

void MainWindow::on_sliderFps_valueChanged(int value)
{
  fps_ = value;
  ui->sliderFps->setSliderPosition(value);
  ui->labelFps->setText(QString("%1 FPS").arg(QString::number(value)));
  UpdateAnalysisInterval();
  QSettings settings;
  settings.setValue("event_flow/fps", value);
}

void MainWindow::on_thresholdLines_valueChanged(int arg1)
{
  QSettings settings;
  settings.setValue("gabor/threshold_lines", arg1);
  emit GuiThresholdChanged(arg1);
}

void MainWindow::on_weight0deg_valueChanged(double arg1)
{
  if (updating_gui_)
    return;
  QSettings settings;
  settings.setValue("gabor/weights/0deg", arg1);
  emit GuiOneWeightChanged(0);
}

void MainWindow::on_weight45deg_valueChanged(double arg1)
{
  if (updating_gui_)
    return;
  QSettings settings;
  settings.setValue("gabor/weights/45deg", arg1);
  emit GuiOneWeightChanged(45);
}

void MainWindow::on_weight90deg_valueChanged(double arg1)
{
  if (updating_gui_)
    return;
  QSettings settings;
  settings.setValue("gabor/weights/90deg", arg1);
  emit GuiOneWeightChanged(90);
}

void MainWindow::on_weight135deg_valueChanged(double arg1)
{
  if (updating_gui_)
    return;
  QSettings settings;
  settings.setValue("gabor/weights/135deg", arg1);
  emit GuiOneWeightChanged(135);
}

void MainWindow::AnalyseScene()
{
  gabor_bank_->UpdateImage(image_events_);
  emit TriggerImageAnalysis();
}

void MainWindow::LoadEdvsDataSuccess(bool ready)
{
  stream_available_ = ready;
  ui->checkBoxStreamEvents->setCheckable(ready);
  ui->checkBoxStreamEvents->setChecked(false);
  QFileInfo file_info(file_path_);
  setWindowTitle("eDVS Shower: " + file_info.fileName());
}

void MainWindow::ShowUiMessage(QString message, int timeout)
{
  ui->statusBar->showMessage(message, timeout);
}

void MainWindow::ShowEventStack(QVector<Edvs::Event> events)
{
  ApplyEventDecay(image_events_, event_alpha_decay_);
  for (int i = 0; i < events.size(); i++)
    if (events.at(i).parity && ui->checkBoxEventsOn->isChecked() ||
        !events.at(i).parity && ui->checkBoxEventsOff->isChecked())
      PaintOneEvent(events.at(i));
  pm_events_ = QPixmap::fromImage(*image_events_);
  UpdateLiveView();
}

// not used
void MainWindow::ShowEvent(Edvs::Event event)
{
  ApplyEventDecay(image_events_, event_alpha_decay_);
  if (event.parity && ui->checkBoxEventsOn->isChecked() ||
      !event.parity && ui->checkBoxEventsOff->isChecked())
    PaintOneEvent(event);
  label_live_view_->setPixmap(QPixmap::fromImage(*image_events_).scaled(edvs_frame_size_));
}

void MainWindow::ShowGaborFilterBank(QImage filter_bank)
{
  ui->GaborFilterBank->setPixmap(QPixmap::fromImage(filter_bank));
}

void MainWindow::ShowLines(QList<line2d> lines)
{
  pm_lines_.fill(Qt::transparent);
  QPainter p (&pm_lines_);
  p.setRenderHint(QPainter::Antialiasing);
  p.setPen(pen_lines_);
  QList<line2d>::const_iterator i;
  for (i = lines.constBegin(); i != lines.constEnd(); ++i) {
      line2d line = (*i);
      int dx = line.filter_size.width() / 2;  // will be automatically rounded down
      int dy = line.filter_size.height() / 2;
      double angle = line.orientation * M_PI / 180;
      QLineF print_line;
      print_line.setP1(line.location +
                       QPoint((int) round(dx * cos(angle)), (int) round(dy * sin(angle))));
      print_line.setLength(line.filter_size.width());
      print_line.setAngle(line.orientation);
      p.drawLine(print_line.toLine());
    }
  p.end();
  UpdateLiveView();
}

/*
 * PRIVATE Methods
 */
void MainWindow::ApplyEventDecay(QImage *image, double decay)
{
  for(int row_index = 0; row_index < edvs_source_size_.height(); row_index++) {
    QRgb *line = (QRgb *)image->scanLine(row_index);
    for(int pix = 0; pix < edvs_source_size_.width(); pix++) {
        QRgb pixel = line[pix];
        line[pix] = qRgba(qRed(pixel),
                          qGreen(pixel),
                          qBlue(pixel),
                          qAlpha(pixel) * decay);
        // TODO: fix decay depending on fps
      }
    }
}

void MainWindow::InitEvents()
{
  image_events_ = new QImage(edvs_source_size_.width(), edvs_source_size_.height(), QImage::Format_ARGB32);
  image_events_->fill(QColor(128, 128, 128, 0));  // transparent gray
  pm_events_ = QPixmap::fromImage(*image_events_);
}

void MainWindow::InitGaborBankTab()
{
  QImage img = QImage(ui->GaborFilterBank->size(), QImage::Format_RGB32);
  img.fill(Qt::gray);
  ui->GaborFilterBank->setPixmap(QPixmap::fromImage(img));
}

void MainWindow::InitLines()
{
  pm_lines_ = QPixmap(edvs_source_size_);
  pm_lines_.fill(Qt::transparent);
}

void MainWindow::InitLiveView()
{
  edvs_frame_size_ = ui->eDvsVisualContainer->size();
  label_live_view_ = new QLabel(ui->eDvsVisualContainer);
  label_live_view_->setStyleSheet("background-color: gray");
}

void MainWindow::InitPen()
{
  pen_lines_ = QPen();
  pen_lines_.setColor(Qt::red);
  pen_lines_.setWidth(2);
  pen_lines_.setCapStyle(Qt::RoundCap);
}

void MainWindow::LoadDefaults(bool force)
{
  QSettings settings;
  defaut_settings ds;
  if (force)
    qDebug() << "Resetting settings.";
  if (!settings.contains("event_flow/fps") || force)
    settings.setValue("event_flow/fps", ds.fps);
  if (!settings.contains("gabor/max_lines") || force)
    settings.setValue("gabor/max_lines", ds.max_lines);
  if (!settings.contains("eDVS_source/resolution") || force)
    settings.setValue("eDVS_source/resolution", ds.edvs_source_size);
  if (!settings.contains("event_flow/alpha_decay") || force)
    settings.setValue("event_flow/alpha_decay", ds.event_alpha_decay);
  if (!settings.contains("gabor/weights/0deg") || force)
    settings.setValue("gabor/weights/0deg", ds.weight0deg);
  if (!settings.contains("gabor/weights/45deg") || force)
    settings.setValue("gabor/weights/45deg", ds.weight45deg);
  if (!settings.contains("gabor/weights/90deg") || force)
    settings.setValue("gabor/weights/90deg", ds.weight90deg);
  if (!settings.contains("gabor/weights/135deg") || force)
    settings.setValue("gabor/weights/135deg", ds.weight135deg);
  if (!settings.contains("gabor/blurC1results") || force)
    settings.setValue("gabor/blurC1results", ds.use_C1_blur);
  if (!settings.contains("gabor/gamma") || force)
    settings.setValue("gabor/gamma", ds.gamma);
  if (!settings.contains("gabor/threshold_lines") || force)
    settings.setValue("gabor/threshold_lines", ds.threshold_lines);
  if (!settings.contains("gabor/max_lines") || force)
    settings.setValue("gabor/max_lines", ds.max_lines);
  if (!settings.contains("gabor/use_manual_weights") || force)
    settings.setValue("gabor/use_manual_weights", ds.use_manual_weights);
  if (!settings.contains("gui/show_filter_results") || force)
    settings.setValue("gui/show_filter_results", ds.show_filter_results);
  InitSettings(force);
}

void MainWindow::InitSettings(bool update_gui)
{
  updating_gui_ = !update_gui;
  QSettings settings;
  max_lines_ = settings.value("gabor/max_lines").toInt();
  edvs_source_size_ = settings.value("eDVS_source/resolution").toSize();
  event_alpha_decay_ = settings.value("event_flow/alpha_decay").toDouble();
  ui->weight0deg->setValue(settings.value("gabor/weights/0deg").toDouble());
  ui->weight45deg->setValue(settings.value("gabor/weights/45deg").toDouble());
  ui->weight90deg->setValue(settings.value("gabor/weights/90deg").toDouble());
  ui->weight135deg->setValue(settings.value("gabor/weights/135deg").toDouble());
  ui->checkBoxBlurC1Results->setChecked(settings.value("gabor/blurC1results").toBool());
  ui->gamma->setValue(settings.value("gabor/gamma").toDouble());
  ui->thresholdLines->setValue(settings.value("gabor/threshold_lines").toInt());
  ui->maxLines->setValue(settings.value("gabor/max_lines").toInt());
  on_rbManualWeights_toggled(settings.value("gabor/use_manual_weights").toBool());
  on_rbShowFilterResults_toggled(settings.value("gui/show_filter_results").toBool());
  on_sliderFps_valueChanged(settings.value("event_flow/fps").toInt());
  updating_gui_ = false;
}

void MainWindow::PaintOneEvent(Edvs::Event event)
{
  QRgb new_color;
  if (event.parity == true) // got brighter
    new_color = qRgb(255, 255, 255);
  else // got darker
    new_color = qRgb(0, 0, 0);
  image_events_->setPixel(event.x, event.y, new_color);
}

void MainWindow::StartEventStream()
{
  ui->checkBoxEventsOn->setEnabled(true);
  ui->checkBoxEventsOn->setChecked(true);
  ui->checkBoxEventsOff->setEnabled(true);
  ui->checkBoxEventsOff->setChecked(true);
  ui->checkBoxAnalyseScene->setEnabled(true);
  ui->checkBoxBlurC1Results->setEnabled(true);
  emit ToggleEmittingEvents(true);
}

void MainWindow::StopEventStream()
{
  ui->checkBoxStreamEvents->setChecked(false);
  ui->checkBoxEventsOn->setEnabled(false);
  ui->checkBoxEventsOn->setChecked(false);
  ui->checkBoxEventsOff->setEnabled(false);
  ui->checkBoxEventsOff->setChecked(false);
  ui->checkBoxAnalyseScene->setEnabled(false);
  ui->checkBoxAnalyseScene->setChecked(false);
  ui->checkBoxBlurC1Results->setEnabled(false);
  emit ToggleEmittingEvents(false);
}

void MainWindow::UpdateAnalysisInterval()
{
  analyse_timer_->setInterval(1000 / fps_ / analysis_freq_ratio_);
}

void MainWindow::UpdateLiveView()
{
  QPixmap pm = QPixmap(edvs_frame_size_);
  pm.fill(Qt::transparent);
  QPainter pt(&pm);
  pt.drawPixmap(0, 0, pm_events_.scaled(edvs_frame_size_));
  pt.drawPixmap(0, 0, pm_lines_.scaled(edvs_frame_size_));
  pt.end();
  label_live_view_->clear();
  label_live_view_->setPixmap(pm);
}

