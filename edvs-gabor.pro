#-------------------------------------------------
#
# Project created by QtCreator 2015-11-13T14:03:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = edvs-gabor
TEMPLATE = app

macx: {
  INCLUDEPATH += /usr/local/Cellar/boost/1.60.0_2/include/ \
    /usr/local/Cellar/opencv3/3.1.0_3/include
}
win32: {

}
unix: {
  INCLUDEPATH += /usr/local/include/opencv-3.0.0/
}

SOURCES += main.cpp\
  mainwindow.cpp \
  lib/parser/edvs_parser.cpp \
  lib/gabor/gabor_filter.cpp \
  lib/gabor/gabor_bank.cpp

HEADERS  += mainwindow.h \
  lib/parser/edvs_parser.h \
  lib/gabor/gabor_filter.h \
  lib/gabor/gabor_bank.h

FORMS    += mainwindow.ui

CONFIG   += c++11 \
            warn_off

macx: LIBS += -L/usr/local/opt/opencv3/lib/ -lopencv_core -lopencv_imgproc
unix: LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgproc
